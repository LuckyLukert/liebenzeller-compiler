# Liebenzeller Compiler

Ein Projekt für BwInf Camps (ursprünglich für das Jugendforum in Bad Liebenzell)

## Links
- [Mars](https://courses.missouristate.edu/KenVollmar/MARS/) ist die beste IDE für MIPS (und unter "Help" gibt's eine Übersicht aller Befehle)
- [MIPS Tutorial](https://minnie.tuhs.org/CompArch/Resources/mips_quick_tutorial.html)


## Grammatik
Die [Backus-Naur-Form](https://en.wikipedia.org/wiki/Backus%E2%80%93Naur_form)(BNF) von der Sprache, mit der wir arbeiten wollen

### Statements
```
s ::= int x; s
    | x = e; s
    | x = input(); s
    | if (e < e) { s } s
    | while (e < e) { s } s
    | print(e); s
    | ε
```

### Expressions
```
e ::= e2 [+ e]
e2 ::= e3 [* e2]
e3 ::= -e3 | e4
e4 ::= n | x | (e)
```
```
n ∈ ℕ
x ∈ Var
```

### Boolsche Ausdrücke
Die lassen wir erstmal weg und fügen sie später hinzu. Wenn es soweit ist: Ersetz die < Kondition in if/while durch ein b
```
b ::= b2 [|| b]
b2 ::= b3 [&& b2]
b3 ::= !b3 | b4
b4 ::= e compare e | (b) | true | false
compare ::= > | > | == | != | <= | >=
```


## Aufgaben
- lern ein wenig **MIPS**. Schreib z.B. ein Beispielprogramm, das zwei Zahlen einliest und `a^b` berechnet
- schau dir `compilerTemplate.py` an (oder übersetzt es in eure Lieblingssprache)
  - vorerst werden wir das Programm direkt als **AST** (abstract syntax tree) eingeben wie unter `#Beispielprogramm` in `compilerTemplate.py`
- schreib für jede Klasse eine `__str__` Methode, damit `print(ast)` etwas sinnvolles ausgibt. z.B. `str(Product(Num(1),Num(2)))` sollte `1*2` ausgeben. Dabei könnt ihr die `str()` Methode von den Unterelementen rekursiv aufrufen.
- schreib für jede Klasse eine `generateCode(self, env)` Methode, die MIPS-Code ausgibt.
  - das `env` ist das **Environment**, in dem du dir nützliche Dinge speichern kannst, wie z.B. welche Variablen schon deklariert wurden und welche MIPS-Labels ihr schon verwendet habt. Leg dir am Besten eine eigene Klasse dafür an
  - `generateCode` sollte **rekursiv** sein
  - Wenn du `generateCode` auf einer Expression aufrufst, sollte der Stack-Pointer `$sp` danach um `4` niedriger sein (4 byte = eine Speicherzelle). Und an der aktuellen `$sp` sollte der Wert der Expression stehen.
- Gratulation! Du hast einen funktionierenden Compiler!


### Extra-Aufgaben
- Schreibe einen **Parser** für die Sprache (Details unten)
- Implementiere **Kommentare** (leicht)
- Implementiere **Else**
- Implementiere **For-Schleifen**
- Implementiere **Boolsche Ausdrücke** (Man muss noch keinen Boolean in eine Variable speichern können)
- Implementiere **Typen** (bool, string, int) (mittelschwer)
  - stell sicher, dass du bei jeder Variable den Typ kennst und du eine Fehlermeldung ausgibst, wenn du bool und int addieren möchtest. Dazu musst du dein Environment anpassen.
  - Erweitere die Input und Print Funktionen, damit sie auch mit Strings und Bools umgehen können
- Implementiere **Funktionen** (schwer)
  - Schreibe die Backus-Naur Formen für Funktionen und Funktionsaufruf auf
  - Integriere sie sie in deinen Parser
  - Dann hör auf zu programmieren!
  - Lies dir diese [Slides](https://lisha.ufsc.br/teaching/sys/stack.pdf) durch und stell sicher, dass du den Aufbau eines Stack-Frames verstanden hast. Wie speicherst du Variablen, die nur innerhalb der Funktion verwendet werden? Tipp: nicht in `.data`. Frag mich zur wenn du dir unsicher bist.
  - [Hier](https://stackoverflow.com/questions/46797915/what-are-the-advantages-of-a-frame-pointer) ist eine gute Erklärung, warum ein frame-pointer sinnvoll ist
  - Mach dir einen genauen Plan, was alles in dein erweitertes Environment muss. Ich hab folgendes gebraucht:
    - ein Speicher, der angibt, ob/in welcher Funktion man gerade ist
    - für jede Funktion:
      - eine Zuordnung von lokalen Variablen zu Offsets vom frame-pointer
      - eine Zuordnung von Argument-Variablen zu Offsets vom frame-pointer
      - der Code für die Funktion, der ganz am Ende des Programms eingefügt wird
  - mach einen genauen Plan was auf dem Stack liegen soll, wenn du eine Funktion aufrufst
  - Gib mir eine kleine Zusammenfassung, was du geplant hast
  - Jetzt darfst du wieder programmieren :)
- **Optimiere** deinen Compiler. 
  - Bei **kleinen Expressions** können wir auf den Stack verzichten und sie nur in den `$s0-s7` Registern berechnen.
  - Schau dir **andere** Compiler-Optimierungen an. Hier ein paar [Beispiele](https://www.quora.com/Why-is-compiler-optimization-important)
  - **function inlining**: Funktionen aufrufen ist teuer. Bei kleinen Funktionen kann es sich lohnen, alle Aufrufe durch den Inhalt der Funktion zu ersetzen



#### Parser
Bisher haben wir dem Compiler immer direkt den AST gegeben. Jetzt ändern wir das. Ein Parser ist eine Komponente, mit der du Text in einen AST umwandelst. Dafür gibt es prinzipiell zwei Möglichkeiten:
1. Du schreibst ihn selbst. Dafür empfehle ich dir, erst einen **Tokenizer** zu schreiben, der den Text in einen Stream von Token umwandelt. Token sind Elemente, wie z.B. `int`, ein Variablenname oder `)`, die schon gruppiert sind und bei denen aller Whitespace entfernt ist.
2. Du verwendest einen Parser-Generator wie [Parsimonious](https://github.com/erikrose/parsimonious). Da musst du nur die Backus-Naur Form angeben und du bekommst eine Funktion, die dir Text in einen `Parse Tree` umwandelt. Diesen `Parse Tree` musst du noch umformen zu dem `AST`, der unsere Klassen verwendet, was du mit einem **Visitor** machen kannst (siehe [Processing Parse Trees](https://github.com/erikrose/parsimonious#processing-parse-trees)).

Hier unsere Grammatik in BNF für Parsimonious:
```python
grammar = Grammar(
	r"""
s      = (decl / assign / input / print / if / while)* w
decl   = w "int " w var w ";"
assign = w var w "=" w e w ";"
input  = w var w "=" w "input()" w ";"
print  = w "print" w "(" e w ")" w ";"
if     = w "if"    w "(" e w "<" e w ")" w "{" s "}"
while  = w "while" w "(" e w "<" e w ")" w "{" s "}"

e      = e2 (w "+" e)?
e2     = e3 (w "*" e2)?
e3     = (w "-" e3) / e4
e4     = num / var / (w "(" e w ")")
num    = w ~"\d+"
var    = w ~"[A-Za-z]+"

w      = ~"\s*"
	""")
```





